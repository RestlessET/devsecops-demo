FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn package
RUN apk add openssh

ENV PORT 5000
EXPOSE $PORT
EXPOSE 22
CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
